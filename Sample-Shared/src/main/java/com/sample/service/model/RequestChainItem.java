package com.sample.service.model;

import java.util.Date;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sample.shared.BasePojo;

public class RequestChainItem extends BasePojo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -372937230675015860L;

	@JsonProperty("chainId")
	private final String chainId;

	@JsonProperty("timestamp")
	private Date requestTimestamp = new Date();

	@JsonProperty("path")
	private String path;

	@JsonProperty("errorMessage")
	private String errorMessage;

	public RequestChainItem() {
		super();
		chainId = generateChainId();
	}

	public RequestChainItem(String pSTChainId) {
		super();
		chainId = pSTChainId;
	}

	public RequestChainItem(String pSTChainId, String pSTPath) {
		super();
		chainId = pSTChainId;
		path = pSTPath;
	}

	public String getChainId() {
		return chainId;
	}

	public Date getRequestTimestamp() {
		return requestTimestamp;
	}

	public void setRequestTimestamp(Date requestTimestamp) {
		this.requestTimestamp = requestTimestamp;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public static String generateChainId() {
		return UUID.randomUUID().toString();
	}

	@Override
	public String toString() {
		StringBuilder lOBStringBuilder = new StringBuilder();
		lOBStringBuilder.append("ChainID: ").append(getChainId());
		lOBStringBuilder.append(" | Timestamp: ").append(getRequestTimestamp().getTime());
		lOBStringBuilder.append(" | Path: ").append(getPath());

		if (getErrorMessage() != null) {
			lOBStringBuilder.append(" | Error Message: ").append(getErrorMessage());
		}

		return lOBStringBuilder.toString();
	}
}
