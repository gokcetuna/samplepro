package com.sample.service.model;

import com.sample.shared.BasePojo;

@SuppressWarnings("rawtypes")
public class ServiceCallerPojo extends BasePojo{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6922329744881337746L;
	
	private String chainId;
	private String url;
	private String mediaType;
	private Class responseClass;

	public ServiceCallerPojo() {
		super();
	}
	
	public ServiceCallerPojo(String chainId, String url, String mediaType, Class responseClass) {
		super();
		this.chainId = chainId;
		this.url = url;
		this.mediaType = mediaType;
		this.responseClass = responseClass;
	}

	public String getChainId() {
		return chainId;
	}

	public void setChainId(String chainId) {
		this.chainId = chainId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMediaType() {
		return mediaType;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	public Class getResponseClass() {
		return responseClass;
	}

	public void setResponseClass(Class responseClass) {
		this.responseClass = responseClass;
	}

}
