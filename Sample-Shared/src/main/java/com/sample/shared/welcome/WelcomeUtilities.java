package com.sample.shared.welcome;

import com.sample.shared.Constants;

public class WelcomeUtilities {

	public static final int TYPE_NAME = 0;
	public static final int TYPE_ID = 1;

	public static final String SLASH = "/";

	private static WelcomeUtilities mOBWelcomeUtilities;

	private WelcomeUtilities() {
		super();
	}

	public static final synchronized WelcomeUtilities getInstance() {
		if (mOBWelcomeUtilities == null) {
			mOBWelcomeUtilities = new WelcomeUtilities();
		}

		return mOBWelcomeUtilities;

	}

	public String getWelcomeServiceUrl(Object pOBParameter, int pinType) {
		StringBuilder lOBUrl = new StringBuilder();

		if (pinType == TYPE_ID) {
			lOBUrl.append(Constants.Services.WelcomeService.WelcomeId.URL);
			lOBUrl.append(SLASH).append((Integer) pOBParameter);
		} else {
			lOBUrl.append(Constants.Services.WelcomeService.Welcome.URL);
			String lSTName = (String) pOBParameter;
			if (lSTName != null && !lSTName.isEmpty()) {
				lOBUrl.append(SLASH).append(lSTName);
			}
		}
		return lOBUrl.toString();
	}

}
