package com.sample.json.convert;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sample.shared.IBasePojo;

public class JSONUtilities {

	public JSONUtilities() {
		super();
	}

	public static String objectToString(IBasePojo pOBObject) throws JsonProcessingException {
		ObjectMapper lOBObjectMapper = new ObjectMapper();
		return lOBObjectMapper.writeValueAsString(pOBObject);
	}

	public static IBasePojo stringToObject(String pSTObject) throws JsonProcessingException, IOException {
		ObjectMapper lOBObjectMapper = new ObjectMapper();
		return (IBasePojo) lOBObjectMapper.reader().readValue(pSTObject);
	}
}