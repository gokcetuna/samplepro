package com.sample.bo;

public abstract class BaseBO implements IBaseBO {

	private final String chainId;

	public BaseBO(String pSTChainId) {
		super();
		this.chainId = pSTChainId;
	}

	@Override
	public String getChainId() {
		return chainId;
	}
}
