package com.sample.bo.welcome;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.sample.bo.BaseBO;
import com.sample.dao.customer.Customer;
import com.sample.dao.customer.CustomerDao;
import com.sample.service.model.RequestChainItem;
import com.sample.shared.Constants;
import com.sample.shared.welcome.WelcomePojo;

abstract class WelcomeBaseBO extends BaseBO implements IWelcomeBO {

	@Autowired
	private CustomerDao mOBCustomerRepository;

	@Autowired
	private Greeting mOBGreeting;

	protected static final Logger LOGGER_CHAIN = LoggerFactory.getLogger(Constants.Literals.LOGGER_CHAIN);

	protected final CustomerDao getCustomerRepository() {
		return mOBCustomerRepository;
	}

	public WelcomeBaseBO(String pSTChainId) {
		super(pSTChainId);
	}

	public WelcomePojo welcome(String pSTName) {
		RequestChainItem lOBRequestChainItem = new RequestChainItem(getChainId(), "WelcomeBO.welcome");
		
		try {
			String lSTMessage = mOBGreeting.getMessage(pSTName);
			return new WelcomePojo(lSTMessage, new Date());
		} finally {
			LOGGER_CHAIN.info(lOBRequestChainItem.toString());
		}
	}

	public WelcomePojo welcomeId(Integer pINId) {
		RequestChainItem lOBRequestChainItem = new RequestChainItem(getChainId(), "WelcomeBO.welcomeId");

		Customer lOBCustomer = null;
		String lSTMessage;
		try {
			lOBCustomer = getCustomerRepository().findOne(pINId);
			if (lOBCustomer != null) {
				lSTMessage = getNameForMessage(lOBCustomer);
			} else {
				lSTMessage = String.valueOf(pINId);
			}
			return welcome(lSTMessage);
		} catch (Exception e) {
			lOBRequestChainItem.setErrorMessage(e.getMessage());
			if (lOBCustomer == null) {
				lSTMessage = "Customer is null. Not found for id: " + pINId;
			} else {
				lSTMessage = e.getMessage();
			}
			return welcome(lSTMessage);
		} finally {
			LOGGER_CHAIN.info(lOBRequestChainItem.toString());
		}
	}

	protected abstract String getNameForMessage(Customer pOBCustomer);

}
