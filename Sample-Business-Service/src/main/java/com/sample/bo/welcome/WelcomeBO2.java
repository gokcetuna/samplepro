package com.sample.bo.welcome;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.sample.dao.customer.Customer;
import com.sample.service.contants.BoNames;

/**
 * 
 * @author Gökçe Tuna
 *
 *         Business object for welcome operations. Test01 feature.
 * 
 */
@Service(BoNames.BO_WELCOME_TEST01)
@Scope("prototype")
class WelcomeBO2 extends WelcomeBaseBO {

	public WelcomeBO2(String pSTChainId) {
		super(pSTChainId);
	}

	@Override
	protected String getNameForMessage(Customer pOBCustomer) {
		StringBuilder lOBStringBuilder = new StringBuilder();
		lOBStringBuilder.append(pOBCustomer.getFirstName()).append(" - ").append(pOBCustomer.getLastName());

		return lOBStringBuilder.toString();
	}
}
