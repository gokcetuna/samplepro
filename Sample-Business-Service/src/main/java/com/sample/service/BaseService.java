package com.sample.service;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.sample.shared.Constants;

public abstract class BaseService {

	@Autowired
	private BusinessObjectFactory mOBBusinessObjectFactory = null;

	@Context
	private HttpHeaders httpHeaders;

	protected static final Logger LOGGER_CHAIN = LoggerFactory.getLogger(Constants.Literals.LOGGER_CHAIN);

	public BaseService() {
		super();
	}

	protected final BusinessObjectFactory getBoFactory() {
		return mOBBusinessObjectFactory;
	}

	protected final HttpHeaders getHttpHeaders() {
		return httpHeaders;
	}

	protected final String getChainId() {
		return httpHeaders.getHeaderString(Constants.HttpHeaders.CHAINID);

	}
}
