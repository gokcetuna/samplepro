package com.sample.ui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.sample.shared.Constants;
import com.sample.shared.context.BeanApplicationContext;

public abstract class BaseController {

	@Autowired
	private BeanApplicationContext mOBApplicationContext;

	protected static final Logger LOGGER_CHAIN = LoggerFactory.getLogger(Constants.Literals.LOGGER_CHAIN);

	public static final String PATH_ROOT = "";

	public BaseController() {
		super();
	}

	public final BeanApplicationContext getApplicationContext() {
		return mOBApplicationContext;
	}

//	public Object getBean(String pSTBeanName) {
//		return getApplicationContext().getBean(pSTBeanName);
//	}

}
