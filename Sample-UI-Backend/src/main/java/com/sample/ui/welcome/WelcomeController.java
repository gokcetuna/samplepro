package com.sample.ui.welcome;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sample.service.model.RequestChainItem;
import com.sample.service.model.ServiceCallerPojo;
import com.sample.shared.Constants;
import com.sample.shared.welcome.WelcomePojo;
import com.sample.shared.welcome.WelcomeUtilities;
import com.sample.ui.BaseController;
import com.sample.ui.BeanNames;
import com.sample.ui.JaxRsCaller;

@Controller(BeanNames.BEAN_WELCOME_CONTROLLER)
@RequestMapping(Constants.Controllers.WelcomeController.PATH)
public class WelcomeController extends BaseController {

	@Autowired
	@Qualifier(BeanNames.BEAN_JAX_RS_CALLER)
	private JaxRsCaller mOBJaxRsCaller;

	protected final JaxRsCaller getJaxRsCaller() {
		return mOBJaxRsCaller;
	}

	/**
	 * Simply selects the home view to render by returning its model object.
	 * 
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST, value = { PATH_ROOT,
			Constants.Controllers.WelcomeController.Welcome.PATH_NAME,
			Constants.Controllers.WelcomeController.Welcome.PATH_ID })
	public Model welcome(Model model,
			@RequestParam(value = Constants.Controllers.WelcomeController.Welcome.PARAM_NAME, required = false) String pSTName,
			@RequestParam(value = Constants.Controllers.WelcomeController.Welcome.PARAM_ID, required = false) Integer pINId)
			throws Exception {

		WelcomePojo lOBWelcomeServiceResponse;

		if (pINId != null) {
			String lSTPath = Constants.Controllers.WelcomeController.PATH
					+ Constants.Controllers.WelcomeController.Welcome.PATH_ID;
			lOBWelcomeServiceResponse = getWelcomeServiceResponse(pINId, WelcomeUtilities.TYPE_ID, lSTPath);

		} else {
			String lSTPath = Constants.Controllers.WelcomeController.PATH
					+ Constants.Controllers.WelcomeController.Welcome.PATH_NAME;

			lOBWelcomeServiceResponse = getWelcomeServiceResponse(pSTName, WelcomeUtilities.TYPE_NAME, lSTPath);
		}
		model.addAttribute(Constants.Controllers.WelcomeController.Welcome.RESPONSE, lOBWelcomeServiceResponse);

		return model;
	}

	/**
	 * 
	 * @param pOBParameter
	 * @param pinType
	 * @return
	 * @throws Exception
	 * 
	 *                   Calls related welcome service and returns the response.
	 */
	private WelcomePojo getWelcomeServiceResponse(Object pOBParameter, int pinType, String pSTPath) throws Exception {

		RequestChainItem lOBRequestChainItem = new RequestChainItem();
		lOBRequestChainItem.setPath(pSTPath);

		String lSTServiceUrl = WelcomeUtilities.getInstance().getWelcomeServiceUrl(pOBParameter, pinType);

		ServiceCallerPojo lSTServiceCallerPojo = new ServiceCallerPojo(lOBRequestChainItem.getChainId(), lSTServiceUrl,
				MediaType.APPLICATION_JSON, WelcomePojo.class);

		// Call welcome rest service
		try {
			return (WelcomePojo) getJaxRsCaller().getResponse(lSTServiceCallerPojo);
		} catch (Exception e) {
			lOBRequestChainItem.setErrorMessage(e.getMessage());
			throw e;
		} finally {
			LOGGER_CHAIN.info(lOBRequestChainItem.toString());
		}

	}

}
