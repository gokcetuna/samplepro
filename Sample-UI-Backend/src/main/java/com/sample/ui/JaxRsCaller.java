package com.sample.ui;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

import org.springframework.aop.target.CommonsPool2TargetSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.sample.service.model.ServiceCallerPojo;
import com.sample.shared.Constants;

@Service(BeanNames.BEAN_JAX_RS_CALLER)
public class JaxRsCaller {

	@Autowired
	@Qualifier(BeanNames.BEAN_JAX_RS_CLIENT_POOL)
	private CommonsPool2TargetSource mOBJaxRsClientPool;

	public JaxRsCaller() {
		super();
	}

	@SuppressWarnings({ "unchecked" })
	public Object getResponse(ServiceCallerPojo pOBServiceCallerPojo) throws Exception {
		Client lOBJaxRsClient = (Client) mOBJaxRsClientPool.getTarget();

		WebTarget lOBWebTarget = lOBJaxRsClient.target(pOBServiceCallerPojo.getUrl());

		Object lOBResponseObject = lOBWebTarget.request(pOBServiceCallerPojo.getMediaType())
				.header(Constants.HttpHeaders.CHAINID, pOBServiceCallerPojo.getChainId())
				.get(pOBServiceCallerPojo.getResponseClass());

		mOBJaxRsClientPool.releaseTarget((Object) lOBJaxRsClient);

		return lOBResponseObject;
	}

//	@SuppressWarnings({ "unchecked" })
//	public Object postResponse(ServiceCallerPojo pOBServiceCallerPojo) throws Exception {
//		Client lOBJaxRsClient = (Client) mOBJaxRsClientPool.getTarget();
//
//		WebTarget lOBWebTarget = lOBJaxRsClient.target(pOBServiceCallerPojo.getUrl());
//
////		Entity lOBEntity = Entity.json(pOBServiceCallerPojo.getChainId());
//
//		Object lOBResponseObject = lOBWebTarget.request(pOBServiceCallerPojo.getMediaType())
//				.header(Constants.HttpHeaders.CHAINID, pOBServiceCallerPojo.getChainId())
//				.get(pOBServiceCallerPojo.getResponseClass());
////				.post(pOBServiceCallerPojo.getResponseClass());
//
//		mOBJaxRsClientPool.releaseTarget((Object) lOBJaxRsClient);
//
//		return lOBResponseObject;
//	}
}
